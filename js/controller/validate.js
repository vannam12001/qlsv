var showMessage = function (id, message) {
  document.getElementById(id).innerHTML = `<strong>${message}</strong>`;
};
var kiemTraTrung = function (ma, dssv) {
  var index = dssv.findIndex(function (item) {
    return ma == item.ma;
  });
  if (index == -1) {
    showMessage("spanMaSV", "");
    return true;
  } else {
    showMessage("spanMaSV", "Mã Sinh Viên không hợp lệ ");
    return false;
  }
};

var kiemTraRong = function (idErr, value) {
  if (value.length == 0) {
    showMessage(idErr, "Ô này không được để trống");
    return false;
  } else {
    showMessage(idErr, "");
    return true;
  }
};

var kiemTraEmail = function () {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    showMessage("spanEmailSV", "");
    return true;
  } else {
    showMessage("spanEmailSV", "email không hợp lệ");

    return false;
  }
};
