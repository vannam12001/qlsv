var dssv = [];
//lấy json lên khi user load trang
var dataJson = localStorage.getItem("DSSV_LOCAL");
//convert ngc lai tu json -> array
if (dataJson != null) {
  dssv = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.ly,
      item.hoa
    );
  }
  console.log("🚀 ` file: main.js:7 ` dssv:", dssv);
  renderDSSV(dssv);
}
function themSinhVien() {
  var sv = layThongTinTuForm();
  var isValid = kiemTraTrung(sv.ma, dssv);
  //tenSV
  isValid = isValid & kiemTraRong("spanTenSV", sv.ten);
  //email
  isValid =
    isValid & kiemTraRong("spanEmailSV", sv.emai) && kiemTraEmail(sv.emai);

  if (isValid) {
    dssv.push(sv);
    //covert array dssv thành Json
    var dataJson = JSON.stringify(dssv);
    //set get remove
    // luu json xuong
    localStorage.setItem("DSSV_LOCAL", dataJson);
    //lay json len trên function themSV
    //render len layout
    renderDSSV(dssv);
  }
}

function xoaSV(id) {
  // timf vị trí
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    if (sv.ma == id) {
      viTri = i;
    }
  }

  dssv.splice(viTri, 1);
  renderDSSV(dssv);
}
function suaSV(id) {
  //console.log("🚀 ` file: main.js:41 ` suaSV ` id:", id);
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  // if (viTri == -1) return;
  if (viTri != -1) {
    // chan user sua ma sv
    document.getElementById("txtMaSV").disabled = true;

    showThongTinLenForm(dssv[viTri]);
  }
}
function capNhatSinhVien() {
  //bo chan user
  document.getElementById("txtMaSV").disabled = false;

  var sv = layThongTinTuForm();
  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  if (viTri !== -1) {
    dssv[viTri] = sv;
    renderDSSV(dssv);
    resetForm();
  }
  // console.log("🚀 ` file: main.js:52 ` capNhatSinhVien ` sv:", sv);
}
function resetForm() {
  document.getElementById("formQLSV").reset();
}
